/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emulinuxcli.commands;

import static emulinuxcli.EmuLinuxCLI.updatePrompt;
import emulinuxcli.File;
import javax.swing.JTextArea;

/**
 *
 * @author christopher
 */
public class cmd_pwd {
    
    public static void pwd(String[] args, File workingDirectory, JTextArea term, StringBuffer sb)
    {
        term.setText(workingDirectory.getPath(sb));
        updatePrompt();
    }
    
}
