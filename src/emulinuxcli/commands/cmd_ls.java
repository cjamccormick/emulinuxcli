/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emulinuxcli.commands;

import java.util.ArrayList;
import javax.swing.JTextArea;
import emulinuxcli.EmuLinuxCLI;
import emulinuxcli.EmuLinuxCLI;
import emulinuxcli.File;

/**
 *
 * @author christopher
 */
public class cmd_ls {
    
    @SuppressWarnings("unchecked")
    public static void ls(String[] args, File workingDirectory, JTextArea term)
    {
        File tmpWD = workingDirectory;
        
        
        if(args.length==2)
        {
            if(args[1].contains("-"))
            {
                if(args[1].contains("l"))
                {
                    StringBuilder sb = new StringBuilder("");
                    
                    if(args[1].contains("a"))
                    {
                        sb.append("drwx------ 65 me me 20480 2020-01-01 9:30 .\n");
                        sb.append("drwxr-xr-x 4 root root 20480 2020-01-01 9:30 ..\n");
                    }
                    
                    ArrayList<File> tempArray;
                    tempArray = workingDirectory.getChildren();

                    
                    for(File f : tempArray)
                    { //todo: see if this should be tabbed
                        if(!f.isHidden() || args[1].contains("a"))
                        {
                            sb.append(f.getPermissions() + " " + f.getNumChildren() + 
                                    " me me " + f.getFileSize() + " " + 
                                    f.getDateModified() + " " + f.getTimeModified() + 
                                    " " + f.getFileName() + "\n");
                        }
                        
                    }

                term.setText(sb.toString());
                }
                
                else if(args[1].contains("a") && !args[1].contains("l"))
                {
                    ArrayList<File> tempArray;
                    tempArray = workingDirectory.getChildren();

                    StringBuilder sb = new StringBuilder("");
                    
                    sb.append(".\t..\t");
        
                    for(File f : tempArray)
                    {
                        sb.append(f.getFileName() + "\t");
                    }
                    term.setText(sb.toString());
                }
                
                else
                {
                    term.setText("ls: invalid option -- '" + args[1] + "'");
                }
                
                return;
            }
            else //path
            {
                String[] temp = new String[2];
                temp[1] = args[1];
                EmuLinuxCLI.cd(temp);
            }
        }
        
        else if(args.length==3)
        {
            String[] temp = new String[2];
            temp[1] = args[2];
            EmuLinuxCLI.cd(temp);
            
            if(args[1].contains("l"))
            {
                ArrayList<File> tempArray;
                tempArray = workingDirectory.getChildren();

                StringBuilder sb = new StringBuilder("");
                    
                if(args[1].contains("a"))
                {
                    sb.append("drwx------ 65 me me 20480 2020-01-01 9:30 .\n");
                    sb.append("drwxr-xr-x 4 root root 20480 2020-01-01 9:30 ..\n");
                }
                    
                for(File f : tempArray)
                { //todo: see if this should be tabbed
                    if(!f.isHidden() || args[1].contains("a"))
                    {
                        sb.append(f.getPermissions() + " " + f.getNumChildren() + 
                                " me me " + f.getFileSize() + " " + 
                                f.getDateModified() + " " + f.getTimeModified() + 
                                " " + f.getFileName() + "\n");
                    }
                }

                term.setText(sb.toString());
                workingDirectory = tmpWD;
                EmuLinuxCLI.updatePrompt();
            }
            
            else if(args[1].contains("a") && !args[1].contains("l"))
                {
                    ArrayList<File> tempArray;
                    tempArray = workingDirectory.getChildren();

                    StringBuilder sb = new StringBuilder("");
                    
                    sb.append(".\t..\t");
        
                    for(File f : tempArray)
                    {
                        sb.append(f.getFileName() + "\t");
                    }
                    
                    term.setText(sb.toString());
                    workingDirectory = tmpWD;
                    EmuLinuxCLI.updatePrompt();
                }
            
            else
            {
                 term.setText("ls: invalid option -- '" + args[1] + "'");
                 workingDirectory = tmpWD;
                 EmuLinuxCLI.updatePrompt();
            }
            return;
        }
        
        ArrayList<File> tempArray;
        tempArray = workingDirectory.getChildren();

        StringBuilder sb = new StringBuilder("");
        for(File f : tempArray)
        {
            if(!f.isHidden())
            {
                sb.append(f.getFileName() + "\t");
            }
            
        }

        term.setText(sb.toString());
        
        workingDirectory = tmpWD;
        EmuLinuxCLI.updatePrompt();
        
        
    }
    
}
