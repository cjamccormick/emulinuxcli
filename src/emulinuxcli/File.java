/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emulinuxcli;

import java.util.ArrayList;

/**
 *
 * @author Christopher McCormick
 */
public class File {
    
    String mFileName;
    String mContent;
    Type mType;
    File mParent;
    ArrayList<File> mChildren = new ArrayList<>();
    
    int mPermUser;
    int mPermGroup;
    int mPermOther;
    
    int mFileSize;
    String mDateModified;
    String mTimeModified;
    
    
    
    //---Constructors---------------------------------------------------------//
    
    File() //root
    {
        mType = Type.Directory;
        mFileName = "/";
        mParent = null;
        mContent = null;
        
        mPermUser = 7;
        mPermGroup = 5;
        mPermOther = 5;
        //todo: tighten these mechanics
        mFileSize = 120143;
        mDateModified = "2020-08-02";
        mTimeModified = "11:54";
    }
    
    File(String name, String content, File parent) //file
    {
        mType = Type.File;
        mFileName = name;
        mParent = parent;
        mContent = content;  
        
        mPermUser = 7;
        mPermGroup = 5;
        mPermOther = 5;
        //todo: tighten these mechanics
        mFileSize = (int)Math.random();
        mDateModified = "2020-08-02";
        mTimeModified = "11:54";
    }
    
    File(String name, Type type, File parent) //directory or link
    {
        mType = type;
        mFileName = name;
        mParent = parent;
        mContent = null; 
        
        mPermUser = 7;
        mPermGroup = 5;
        mPermOther = 5;
        //todo: tighten these mechanics
        mFileSize = (int)Math.random();
        mDateModified = "2020-08-02";
        mTimeModified = "11:54";
    }
    
    
    
    //---Enums----------------------------------------------------------------//
    
    enum Type
    {
        File, Directory, Link
    }
    
    
    
    //---Getters-and-Setters--------------------------------------------------//
    
    public String getContent()
    {
        return mContent;
    }
    
    public void setContent(String s)
    {
        mContent = s;
    }
    
    public File getParent()
    {
        return mParent;
    }
    
    public void setParent(File file)
    {
        mParent = file;
    }
    
    public String getFileName()
    {
        return mFileName;
    }
    
    public void setFileName(String s)
    {
        mFileName = s;
    }
    
    public String getPathName()
    {
        return "/" + mFileName;
    }
    
    public void setPathName(String s)
    {
        mFileName = "/" + s;
    }
    
    public int getNumChildren()
    {
        return mChildren.size();
    }
    
    public int getFileSize()
    {
        return mFileSize;
    }
    
    public void setFileSize(int i)
    {
        mFileSize = i;
    }
    
    public String getDateModified()
    {
        return mDateModified;
    }
    
    public void setDateModified()
    {
        //todo: code for set date
    }
    
    public String getTimeModified()
    {
        return mTimeModified;
    }
    
    public void setTimeModified()
    {
        //todo: code for set time
    }
    
    
    //---Methods--------------------------------------------------------------//
    
    public void addChild(File file)
    {
        mChildren.add(file);
    }
    
    public ArrayList getChildren()
    {
        return mChildren;
    }
    
    public String getPath(StringBuffer sb)
    {
        if(mFileName=="/")
            return mFileName;
        
        if(mParent!=null)
        {
            sb.delete(0, sb.length());
            mParent.getPath(sb);
        }
        sb.append(this.getPathName());
        return sb.toString();        
    }
    
    public String getPermissions()
    {
        StringBuffer sb = new StringBuffer();
        
        switch(mType)
        {
            case File:
                sb.append("-");
                break;
            case Directory:
                sb.append("d");
                break;
            case Link:
                sb.append("l");
                break;
            default:
                sb.append("ERROR_");
        }
        
        sb.append(OctalConversion(mPermUser));
        sb.append(OctalConversion(mPermGroup));
        sb.append(OctalConversion(mPermOther));
        
        return sb.toString();
    }
    
    public String OctalConversion(int i)
    {
        switch(i)
        {
            case 0:
                return "---";
            case 1:
                return "--x";
            case 2:
                return "-w-";
            case 3:
                return "-wx";
            case 4:
                return "r--";
            case 5:
                return "r-x";
            case 6:
                return "rw-";
            case 7:
                return "rwx";
            default:
                return "_ERROR_";                
        }
    }
    
    public boolean isHidden()
    {
        if (mFileName.startsWith("."))
            return true;
        else
            return false;
    }
}
