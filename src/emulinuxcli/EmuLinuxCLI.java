/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emulinuxcli;

import emulinuxcli.File.Type;
import emulinuxcli.commands.cmd_ls;
import emulinuxcli.commands.cmd_pwd;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


/**
 *
 * @author Christopher McCormick
 */
public class EmuLinuxCLI implements ActionListener {

    private static File root = new File();
    private static File workingDirectory;
    private static ArrayList<File> files = new ArrayList<>();
    
    //---
    private static File bin = new File("bin", Type.Directory, root);
    private static File usr = new File("usr", Type.Directory,  root);
    private static File bin2 = new File("bin", Type.Directory,  usr);
    private static File temp = new File("text.txt", "Hello World", bin2);
    private static File home = new File("home", Type.Directory, root);
    private static File me = new File("me", Type.Directory, home);
    private static File h_cache = new File(".cache", Type.Directory, root);
    private static File h_temp = new File(".other", Type.Directory, home);
    //---
    
    private static StringBuffer sb = new StringBuffer();
    private static StringBuffer tmpCMD = new StringBuffer();
    private GridBagConstraints constraints = new GridBagConstraints();
    private Font terminalFont = new Font("TimesRoman", Font.PLAIN, 20);

    private static JFrame frame = new JFrame();
    private JPanel panel = new JPanel();
    
    private static JTextField command = new JTextField();
    private static JTextField prompt = new JTextField();
    private static JTextArea term = new JTextArea();
    
    EmuLinuxCLI()
    {
        workingDirectory = me;
        updatePrompt();
        
        panel.setLayout(new GridBagLayout());
        panel.setBackground(Color.black);
        
        
        prompt.setBackground(Color.black);
        prompt.setForeground(Color.green);
        prompt.setFont(terminalFont);
        prompt.setEditable(false);
        
        
        command.setBackground(Color.black);
        command.setForeground(Color.white);
        command.setFont(terminalFont);
        command.addActionListener(this);
        
       
        term.setFont(terminalFont);
        term.setBackground(Color.black);
        term.setForeground(Color.white);
        term.setLineWrap(true);
        term.setEditable(true);
        term.setCaretPosition(term.getDocument().getLength());
        term.setEditable(false);
        
        JScrollPane log = new JScrollPane(term);
        log.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;        
        
        constraints.weightx = 25;
        constraints.weighty = 5;
        constraints.gridx = 0;
        constraints.gridy = 0;
        panel.add(prompt, constraints);
        
        
        
        constraints.weightx = 75;
        constraints.weighty = 5;
        constraints.gridx = 1;
        constraints.gridy = 0;
        panel.add(command, constraints);
        
        
        
        constraints.weightx = 100;
        constraints.weighty = 95;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        panel.add(log, constraints);
        


        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        
    }
    
    public void actionPerformed(ActionEvent ae) 
        {
            Object eventSource = ae.getSource();
        
            if(eventSource == command) 
            {
                String[] args = command.getText().split(" ");
                
                tmpCMD.delete(0, tmpCMD.length());
                tmpCMD.append(command.getText());
                
                command.setText("");
                term.setText("");
                identify(args);
            }
        }
    
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        EmuLinuxCLI linuxCLI = new EmuLinuxCLI();
        frame.setTitle("Emulated Linux Command Line Interface");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
        root.addChild(bin);
        root.addChild(usr);
        usr.addChild(bin2);
        root.addChild(home);
        home.addChild(me);
        root.addChild(h_cache);
        home.addChild(h_temp);
        
        System.out.println(temp.getPermissions());
    }
    
    public static void updatePrompt()
    {
        if(workingDirectory == me)
            prompt.setText("me@linuxbox:~$");
        else
            prompt.setText("me@linuxbox:" + workingDirectory.getPath(new StringBuffer()) + "$");
    }
                
    
    public void identify(String[] args)
    {
        if(args[0].equals("pwd"))
            cmd_pwd.pwd(args, workingDirectory, term, new StringBuffer());
        else if(args[0].equals("cd"))
            cd(args);
        else if(args[0].equals("ls"))
            cmd_ls.ls(args, workingDirectory, term);
        else
            term.setText("");
    }
    
    
    
    
    
    
    @SuppressWarnings("unchecked")
    public static void cd(String[] args)
    {
        if(args.length==1)
        {
            workingDirectory = me;
            updatePrompt();
            return;
        }
        else if(args.length > 2)
        {
            term.setText("bash: cd: too many arguments");
        }
            
        
        File tmpWD = workingDirectory;
        String[] paths = args[1].split("/");
        
        for(String s : paths)
        {
            System.out.println(s);
            if (s.equals(".."))
            {
                if(tmpWD != root)
                {
                    tmpWD = tmpWD.mParent;
                    continue;
                }
            }
            
            ArrayList<File> children;
            children = tmpWD.getChildren();
            for(File f : children)
            {
                if(s.equals(f.getFileName()))
                {
                    tmpWD = f;
                    break;
                }
            }
        }
        
        if(tmpWD != workingDirectory)
        {
            workingDirectory = tmpWD;
            updatePrompt();
        }
            
        else
            term.setText("bash: " + tmpCMD.toString() + ": No such file or directory");
            
    }
    
    
    
    
}

